FROM keymetrics/pm2:10-alpine

RUN mkdir /app
WORKDIR /app
COPY . .

RUN npm ci
RUN npm run build -- --prod

EXPOSE 8080

CMD ["pm2-runtime", "start", "ecosystem.config.js"]
