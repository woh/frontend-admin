const express = require('express');
const history = require('connect-history-api-fallback');

const app = express();
const port = process.env.PORT || 8080;

app.use(history());
app.use(express.static('docs'));

app.listen(port, '0.0.0.0', () => console.log(`Listening ${port}!`));
