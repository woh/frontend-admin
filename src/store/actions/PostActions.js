export const POST_SAVE = 'POST_SAVE';
export const POST_ADD = 'POST_ADD';
export const POST_DELETE = 'POST_DELETE';
export const POST_SUCCESS = 'POST_SUCCESS';
export const POST_ALLOW = 'POST_ALLOW';
export const POST_DISALLOW = 'POST_DISALLOW';
export const POST_LIKE = 'POST_LIKE';
export const POST_DISLIKE = 'POST_DISLIKE';
export const YOUTUBE_GET = 'YOUTUBE_GET';

