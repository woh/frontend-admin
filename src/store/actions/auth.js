export const AUTH_REQUEST = 'AUTH_REQUEST';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_ERROR = 'AUTH_ERROR';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const AUTH_REGISTRATION = 'AUTH_REGISTRATION';
export const AUTH_REG_DONE = 'AUTH_REG_DONE';
export const AUTH_REG_ERROR = 'AUTH_REG_ERROR';
export const AUTH_SHOW = 'AUTH_SHOW';
