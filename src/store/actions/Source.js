export const SOURCES_GET = 'SOURCES_GET';
export const SOURCE_GET = 'SOURCE_GET';
export const SOURCE_RUN = 'SOURCE_RUN';
export const SOURCE_ADD = 'SOURCE_ADD';
export const SOURCE_SAVE = 'SOURCE_SAVE';
export const SOURCE_DELETE = 'SOURCE_DELETE';
export const SOURCE_ERROR = 'SOURCE_ERROR';
