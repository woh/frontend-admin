import Vue from 'vue';
import Vuex from 'vuex';
import Comments from './modules/comments';
import User from './modules/user';
import Auth from './modules/auth';
import PostGet from './modules/PostGet';
import PostActions from './modules/PostActions';
import Source from './modules/Source';
import SourcePreview from './modules/SourcePreview';
import Teaser from './modules/Teaser';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Comments,
    User,
    Auth,
    PostGet,
    PostActions,
    Source,
    SourcePreview,
    Teaser,
  },
});
