/* eslint-disable no-shadow,no-param-reassign,no-console */
import axios from 'axios';
import Vue from 'vue';

import {
  TEASERS_REQUEST, TEASERS_SUCCESS, TEASER_SAVE, TEASER_DELETE,
  TEASERS_BY_POST_ID_REQUEST, TEASERS_BY_POST_ID_SUCCESS,
} from '../actions/Teaser';

const state = {
  teasers: {
    teasersLoading: false,
    teasersAll: [],
    teasers: [],
    featured: [],
    teaserView: {},
    featuredView: [],
    teaserLastEndTime: '',
    featuredLastEndTime: '',
  },
  teasersByPostId: {
    teasersLoading: false,
    teasersAll: [],
    teasers: [],
    featured: [],
  },
};

const getters = {
  teasers: state => state.teasers,
  teasersByPostId: state => state.teasersByPostId,
};

const actions = {
  [TEASERS_REQUEST]: ({ commit }) => {
    commit(TEASERS_REQUEST);
    axios({ url: `${process.env.apiUrl}/teasers/all/`, method: 'GET' })
      .then((response) => {
        commit(TEASERS_SUCCESS, response);
      })
      .catch((err) => {
        console.log('Error :-S', err);
      });
  },

  [TEASERS_BY_POST_ID_REQUEST]: ({ commit }, id) => {
    commit(TEASERS_BY_POST_ID_REQUEST);
    axios({ url: `${process.env.apiUrl}/teasers/${id}/`, method: 'GET' })
      .then((response) => {
        commit(TEASERS_BY_POST_ID_SUCCESS, response);
      })
      .catch((err) => {
        console.log('Error :-S', err);
      });
  },

  [TEASER_SAVE]: ({ commit }, teaser) =>
    axios({ url: `${process.env.apiUrl}/teasers/`, data: teaser, method: 'POST' })
      .then((response) => {
        commit(TEASER_SAVE, response);
      })
      .catch((err) => {
        console.log('Error :-S', err);
      }),
  [TEASER_DELETE]: ({ commit }, teaser) =>
    axios({ url: `${process.env.apiUrl}/teasers/delete/`, data: teaser, method: 'POST' })
      .then((response) => {
        commit(TEASER_DELETE, response);
      })
      .catch((err) => {
        console.log('Error :-S', err);
      }),
};

const mutations = {
  [TEASERS_REQUEST]: (state) => {
    state.teasers.teasersLoading = false;
  },
  [TEASERS_SUCCESS]: (state, response) => {
    state.teasers.teasersAll = response.data.items;

    state.teasers.teasers = state.teasers.teasersAll.filter(teaser => teaser.type === 'TYPE_TEASER');
    state.teasers.featured = state.teasers.teasersAll.filter(teaser => teaser.type === 'TYPE_FEATURE');

    Vue.set(state.teasers, 'teaserView', state.teasers.teasers[0]);
    state.teasers.featuredView = state.teasers.featured.filter((featured, index) => index < 2);

    state.teasers.teaserLastEndTime = state.teasers.teasers[state.teasers.teasers.length - 1].teaserTo;
    state.teasers.featuredLastEndTime = state.teasers.featured[state.teasers.featured.length - 1].teaserTo;
    state.teasers.teasersLoading = true;
  },

  [TEASERS_BY_POST_ID_REQUEST]: (state) => {
    state.teasersByPostId.teasersLoading = false;
  },
  [TEASERS_BY_POST_ID_SUCCESS]: (state, response) => {
    state.teasersByPostId.teasersAll = response.data;

    state.teasersByPostId.teasers = state.teasersByPostId.teasersAll.filter(teaser => teaser.type === 'TYPE_TEASER');
    state.teasersByPostId.featured = state.teasersByPostId.teasersAll.filter(teaser => teaser.type === 'TYPE_FEATURE');

    state.teasersByPostId.teasersLoading = true;
  },

  [TEASER_DELETE]: (state, response) => {
    console.log(response.data);
  },
  [TEASER_SAVE]: () => {
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
