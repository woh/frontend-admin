/* eslint-disable no-shadow,no-console,no-param-reassign,no-unused-vars */
import axios from 'axios';
import {
  SOURCE_ADD, SOURCE_DELETE, SOURCE_SAVE, SOURCE_GET,
  SOURCE_RUN, SOURCES_GET,
} from '../actions/Source';

const state = {
  sources: [], source: {}, sourceLoading: false,
};

const getters = {
  sources: state => state.sources,
  source: state => state.source,
  sourceLoading: state => state.sourceLoading,
  newSourceId: state => state.source.id,
};

const actions = {
  [SOURCES_GET]: ({ commit }) => {
    axios({ url: `${process.env.apiUrl}/source/`, method: 'GET' })
      .then((response) => {
        commit(SOURCES_GET, response);
      })
      .catch((err) => {
        console.log(err);
      });
  },
  [SOURCE_GET]: ({ commit }, id) => {
    state.sourceLoading = true;
    axios({ url: `${process.env.apiUrl}/source/${id}/`, method: 'GET' })
      .then((response) => {
        commit(SOURCE_GET, response);
      })
      .catch((err) => {
        console.log(err);
      });
  },
  [SOURCE_RUN]: ({ commit }, id) => {
    axios({ url: `${process.env.apiUrl}/source/run/${id}/`, method: 'GET' })
      .then(() => {
        console.log('всё ок');
      })
      .catch((err) => {
        console.log(err);
      });
  },
  [SOURCE_ADD]: ({ commit }, source) =>
    axios({ url: `${process.env.apiUrl}/source/add/`, data: source, method: 'POST' })
      .then((response) => {
        commit(SOURCE_ADD, response);
      })
      .catch((err) => {
        console.log(err);
      }),
  [SOURCE_SAVE]: ({ commit }, source) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/source/edit/`, data: source, method: 'POST' })
        .then((response) => {
          commit(SOURCE_GET, response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
  [SOURCE_DELETE]: ({ commit }, id) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/source/delete/${id}/`, method: 'POST' })
        .then(() => {
          resolve();
        })
        .catch((err) => {
          reject(err);
        });
    }),
};

const mutations = {
  [SOURCES_GET]: (state, response) => {
    state.sources = response.data;
    state.sources.unshift({ id: 0, name: 'Все источники' });
  },
  [SOURCE_GET]: (state, response) => {
    state.source = response.data;
    state.sourceLoading = false;
  },
  [SOURCE_ADD]: (state, response) => {
    state.source = response.data;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
