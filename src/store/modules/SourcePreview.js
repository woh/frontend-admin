/* eslint-disable no-shadow,no-console,no-param-reassign */
import axios from 'axios';
import { POST_PREVIEW, POSTS_PREVIEW_BS, POSTS_PREVIEW } from '../actions/SourcePreview';

const state = {
  postsAll: [], posts: [], post: {},
};

const getters = {
  postsAllPreview: state => state.postsAll,
  postsPreview: state => state.posts,
  postPreview: state => state.post,
};

const actions = {
  [POSTS_PREVIEW]: ({ commit }) => {
    axios({ url: `${process.env.apiUrl}/post-preview/`, method: 'GET' })
      .then((response) => {
        commit(POSTS_PREVIEW, response);
      })
      .catch((err) => {
        console.log(err);
      });
  },
  [POSTS_PREVIEW_BS]: ({ commit }, id) => {
    axios({ url: `${process.env.apiUrl}/post-preview/by-source/${id}/`, method: 'GET' })
      .then((response) => {
        commit(POSTS_PREVIEW_BS, response);
      })
      .catch((err) => {
        console.log(err);
      });
  },
  [POST_PREVIEW]: ({ commit }, id) => {
    axios({ url: `${process.env.apiUrl}/post-preview/${id}/`, method: 'GET' })
      .then((response) => {
        commit(POST_PREVIEW, response);
      })
      .catch((err) => {
        console.log(err);
      });
  },
};

const mutations = {
  [POSTS_PREVIEW]: (state, response) => {
    state.postsAll = response.data;
    console.log('всё', response);
  },
  [POSTS_PREVIEW_BS]: (state, response) => {
    state.posts = response.data;
    console.log('бай сурс', response);
  },
  [POST_PREVIEW]: (state, response) => {
    state.post = response.data;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
