/* eslint-disable no-shadow,no-param-reassign,no-unused-vars,no-console */
import Vue from 'vue';
import axios from 'axios';
import {
  USER_REQUEST, USER_ERROR, USER_SUCCESS,
  AVATAR_SAVE, AVATAR_DEL, USER_SAVE, USER_SAVE_PASS, USER_REQUEST_BY_ID, USER_SUCCESS_BY_ID, USER_SAVE_BY_ID,
} from '../actions/user';
import { AUTH_LOGOUT } from '../actions/auth';

const state = { status: '', user: {}, userById: {} };

const getters = {
  user: state => state.user,
  userById: state => state.userById,
  isProfileLoaded: state => !!state.user.email,
};

const actions = {

  [USER_REQUEST]: ({ commit, dispatch }) =>
    new Promise((resolve, reject) => {
      commit(USER_REQUEST);
      axios({ url: `${process.env.apiUrl}/user`, method: 'GET' })
        .then((response) => {
          commit(USER_SUCCESS, response);
          resolve(response);
        })
        .catch((err) => {
          commit(USER_ERROR);
          // if resp is unauthorized, logout, to
          reject(err);
        });
    }),
  [USER_REQUEST_BY_ID]: ({ commit, dispatch }, id) =>
    new Promise((resolve, reject) => {
      commit(USER_REQUEST_BY_ID);
      axios({ url: `${process.env.apiUrl}/user/${id}/`, method: 'GET' })
        .then((response) => {
          commit(USER_SUCCESS_BY_ID, response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
  [AVATAR_SAVE]: ({ commit, dispatch }, avatar) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/user/avatar/`, data: avatar, method: 'POST' })
        .then((response) => {
          commit(AVATAR_SAVE, response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),

  [AVATAR_DEL]: ({ commit, dispatch }) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/user/avatar/drop/`, method: 'POST' })
        .then((response) => {
          dispatch(USER_REQUEST);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),

  [USER_SAVE]: ({ commit, dispatch }, user) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/user/save/`, data: user, method: 'POST' })
        .then((response) => {
          if (!user.adminEdit) {
            commit(USER_SUCCESS, response);
          } else {
            commit(USER_SUCCESS_BY_ID, response);
          }
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
  [USER_SAVE_PASS]: ({ commit, dispatch }, pass) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/user/password`, data: pass, method: 'POST' })
        .then((response) => {
          commit(USER_SAVE_PASS, response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
};

const mutations = {
  [USER_REQUEST]: (state) => {
    state.status = 'loading';
  },
  [USER_REQUEST_BY_ID]: (state) => {
  },
  [USER_SUCCESS]: (state, response) => {
    state.user = response.data;
    if (state.user.avatar) {
      state.user.avatar = `${process.env.imgRoot}${state.user.avatar}`;
    }
    state.user.isMailConfirmed = state.user.isMailConfirmed || false;
    state.user.access = state.user.role === 'ROLE_ADMIN' || state.user.role === 'ROLE_MODER';
    state.status = 'success';
  },
  [USER_SUCCESS_BY_ID]: (state, response) => {
    state.userById = response.data;
    if (state.userById.avatar) {
      state.userById.avatar = `${process.env.imgRoot}${state.userById.avatar}`;
    }
    state.userById.isMailConfirmed = state.userById.isMailConfirmed || false;
    state.userById.access = state.userById.role === 'ROLE_ADMIN' || state.userById.role === 'ROLE_MODER';
  },
  [USER_ERROR]: (state) => {
    state.status = 'error';
  },
  [AUTH_LOGOUT]: (state) => {
    state.user = {};
  },
  [AVATAR_SAVE]: (state, response) => {
    state.user.avatar = `${process.env.imgRoot}${response.data}`;
  },
  [USER_SAVE]: (state, response) => {
  },
  [USER_SAVE_PASS]: (state, response) => {
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
