/* eslint-disable no-shadow,no-param-reassign,no-unused-vars,no-console */
import axios from 'axios';
import {
  POST_ADD,
  POST_DELETE,
  POST_SAVE,
  POST_ALLOW,
  POST_DISALLOW,
  YOUTUBE_GET,
  POST_DISLIKE,
  POST_LIKE,
} from '../actions/PostActions';
import { POST_SUCCESS } from '../actions/PostGet';
import PostGet from './PostGet';

const state = { post: {}, youtube: {} };

const getters = {
  newPostId: state => state.post.id,
  youtube: state => state.youtube,
};

const actions = {
  [POST_ADD]: ({ commit, dispatch }, post) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/add`, data: post, method: 'POST' })
        .then((response) => {
          commit(POST_ADD, response);
          resolve(response);
        })
        .catch((err) => {
          console.log('Error :-S', err);
          reject(err);
        });
    }),

  [POST_DELETE]: ({ commit, dispatch }, id) => {
    axios({ url: `${process.env.apiUrl}/${id}/delete`, method: 'POST' })
      .then(() => {
      })
      .catch((err) => {
      });
  },

  [POST_SAVE]: ({ commit }, post) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/${post.id}`, data: post, method: 'POST' })
        .then((response) => {
          commit(POST_SUCCESS, response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),

  [POST_ALLOW]: ({ commit, dispatch }, id) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/${id}/approve`, method: 'POST' })
        .then((response) => {
          commit(POST_ALLOW, response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
  [POST_DISALLOW]: ({ commit, dispatch }, id) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/${id}/dismiss`, method: 'POST' })
        .then((response) => {
          commit(POST_DISALLOW, response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
  [POST_LIKE]: ({ commit, dispatch }, id) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/${id}/like/`, method: 'POST' })
        .then((response) => {
          commit(POST_SUCCESS, response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
  [POST_DISLIKE]: ({ commit, dispatch }, id) =>
    new Promise((resolve, reject) => {
      axios({ url: `${process.env.apiUrl}/${id}/dislike/`, method: 'POST' })
        .then((response) => {
          commit(POST_SUCCESS, response);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    }),
  [YOUTUBE_GET]: ({ commit }, id) =>
    axios({ url: `${process.env.apiUrl}/youtube/${id}` })
      .then((response) => {
        commit(YOUTUBE_GET, response);
      })
      .catch((err) => {
        console.log(err);
      }),
};

const mutations = {
  [POST_ADD]: (state, response) => {
    state.post = response.data;
  },
  [POST_SAVE]: (state, response) => {
    state.post = response.data;
  },
  [POST_ALLOW]: (state, response) => {
    PostGet.state.post.isAllowed = response.data.isAllowed;
  },
  [POST_DISALLOW]: (state, response) => {
    PostGet.state.post.isAllowed = response.data.isAllowed;
  },
  [YOUTUBE_GET]: (state, response) => {
    state.youtube = response.data;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
