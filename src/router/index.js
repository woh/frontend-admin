/* eslint-disable no-alert */
import Vue from 'vue';
import Router from 'vue-router';

// Components
import TeasersList from '../components/admin-page/admin-posts-manager/TeasersList';
import AdminPostManager from '../components/admin-page/admin-posts-manager/AdminPostManager';
import AdminPublished from '../components/admin-page/admin-posts-manager/AdminPublished';
import AdminPublishedWaiting from '../components/admin-page/admin-posts-manager/AdminPublishedWaiting';
import AdminModerationWaiting from '../components/admin-page/admin-posts-manager/AdminModerationWaiting';
import AdminWowArticle from '../components/admin-page/admin-posts-manager/AdminWowArticle';
import PostsList from '../components/posts/PostsList';
import PostsByTag from '../components/posts/PostsByTag';
import PostsByCategory from '../components/posts/PostsByCategory';
import Login from '../components/parts/Login';
import PostDetails from '../components/posts/PostDetails';
import PostEdit from '../components/admin-page/post-edit/PostEdit';
import PostAdd from '../components/trash/PostAdd';
import User2 from '../components/trash/user';
import User from '../components/user-profile/User';
import UserNewPost from '../components/user-profile/UserNewPost';
import UserPostsList from '../components/user-profile/UserPostsList';
import UserSettings from '../components/user-profile/UserSettings';
import SourcesList from '../components/admin-page/source/SourcesList';
import SourceAdd from '../components/admin-page/source/SourceAdd';
import SourceSettings from '../components/admin-page/source/SourceSettings';
import UserAdministration from '../components/admin-page/user-administration/UserAdministration';
import NotFoundComponent from '../components/NotFoundComponent';
import TestPage from '../components/TestPage';
import WoWHome from '../components/WoW/WowHome';
import store from '../store';

Vue.use(Router);

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next();
    return;
  }
  next(`${process.env.baseUrl}/`);
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next();
    return;
  }
  next(`${process.env.baseUrl}/login`);
};

export default new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: [
    {
      name: 'posts-list', path: `${process.env.baseUrl}`, component: PostsList, beforeEnter: ifAuthenticated,
    },
    {
      path: `${process.env.baseUrl}/user`,
      component: User,
      meta: { menu: 'side-bar-user-page' },
      children: [
        {
          name: 'user-posts-list', path: '', component: UserPostsList, meta: { menu: 'side-bar-user-page' },
        },
        {
          name: 'user-new-post', path: `${process.env.baseUrl}/user-new-post`, component: UserNewPost, meta: { menu: 'side-bar-user-page' },
        },
        {
          name: 'user-settings', path: `${process.env.baseUrl}/user-settings`, component: UserSettings, meta: { menu: 'side-bar-user-page' },
        },
      ],
      beforeEnter: ifAuthenticated,
    },
    {
      name: 'user2', path: `${process.env.baseUrl}/user2`, component: User2, beforeEnter: ifAuthenticated,
    },
    {
      name: 'wow-home', path: `${process.env.baseUrl}/wow`, component: WoWHome, meta: { layout: 'WowLayout' }, beforeEnter: ifAuthenticated,
    },
    {
      name: 'posts-by-tag', path: `${process.env.baseUrl}/tag/:tag`, component: PostsByTag,
    },
    {
      path: `${process.env.baseUrl}/admin-post-manager`,
      component: AdminPostManager,
      meta: { menu: 'sidebar-admin' },
      children: [
        { path: '', component: AdminPublished, meta: { menu: 'sidebar-admin' } },
        { path: 'published-waiting', component: AdminPublishedWaiting, meta: { menu: 'sidebar-admin' } },
        { path: 'moderating-waiting', component: AdminModerationWaiting, meta: { menu: 'sidebar-admin' } },
        { path: 'teaser-list', component: TeasersList, meta: { menu: 'sidebar-admin' } },
        { path: 'wow-article', component: AdminWowArticle, meta: { menu: 'sidebar-admin' } },
      ],
      beforeEnter: ifAuthenticated,
    },
    {
      name: 'sources-list', path: `${process.env.baseUrl}/source-list`, component: SourcesList, meta: { menu: 'sidebar-admin' }, beforeEnter: ifAuthenticated,
    },
    {
      name: 'new-source', path: `${process.env.baseUrl}/new-source`, component: SourceAdd, meta: { menu: 'sidebar-admin' }, beforeEnter: ifAuthenticated,
    },
    {
      name: 'source-settings', path: `${process.env.baseUrl}/source/:id`, component: SourceSettings, meta: { menu: 'sidebar-admin' }, beforeEnter: ifAuthenticated,
    },
    {
      name: 'new-post', path: `${process.env.baseUrl}/new-post`, component: PostAdd, meta: { menu: 'sidebar-admin' }, beforeEnter: ifAuthenticated,
    },
    {
      name: 'post-edit', path: `${process.env.baseUrl}/edit/:id`, component: PostEdit, meta: { menu: 'sidebar-admin' }, beforeEnter: ifAuthenticated,
    },
    {
      name: 'user-administration-id', path: `${process.env.baseUrl}/user-administration/:id`, component: UserAdministration, meta: { menu: 'sidebar-admin' }, beforeEnter: ifAuthenticated,
    },
    {
      name: 'user-administration', path: `${process.env.baseUrl}/user-administration`, component: UserAdministration, meta: { menu: 'sidebar-admin' }, beforeEnter: ifAuthenticated,
    },
    {
      name: 'login', path: `${process.env.baseUrl}/login`, component: Login, meta: { layout: 'WhiteBlank' }, beforeEnter: ifNotAuthenticated,
    },
    {
      name: 'test', path: `${process.env.baseUrl}/test`, component: TestPage, beforeEnter: ifAuthenticated,
    },
    {
      name: 'posts-by-category-sub', path: `${process.env.baseUrl}/category-list/:category/:sub`, component: PostsByCategory, beforeEnter: ifAuthenticated,
    },
    {
      name: 'posts-by-category', path: `${process.env.baseUrl}/category-list/:category`, component: PostsByCategory, beforeEnter: ifAuthenticated,
    },
    {
      name: 'post-details', path: `${process.env.baseUrl}/category-list/:category/post/:id`, component: PostDetails, beforeEnter: ifAuthenticated,
    },
    { path: `${process.env.baseUrl}/*`, component: NotFoundComponent, beforeEnter: ifAuthenticated },
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    return { x: 0, y: 0 };
  },
});
