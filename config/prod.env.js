module.exports = {
  NODE_ENV: '"production"',
  apiUrl: process.env.API_URL || "'/api'",
  imgRoot: process.env.IMG_ROOT || "''",
  imgProxy: process.env.IMG_PROXY || "'https://beta.woh.ru:3003/'",
  baseUrl: process.env.BASE_URL || "'/admin'"
}
